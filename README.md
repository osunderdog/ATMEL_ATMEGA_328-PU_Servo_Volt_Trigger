# ATMEL_ATMEGA_328-PU_Servo_Volt_Trigger
ATMEGA code to trigger a server turn based on a change in a voltage.

This will be used to trigger a AWS IOT button.
Push button is defined as servo rotating from at rest set point angle to push set point angle.
When voltage increases above a set point, the servo will push button once.
When voltage decreases below the set point, the serveo will push button twice
Pictures and full description will be in a post later.

Uses make system and libraries described in the book:
MAKE: AVR Programming
by: Elliot Williams

