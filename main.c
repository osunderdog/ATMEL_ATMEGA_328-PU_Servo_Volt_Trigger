
#include "main.h" 

// -------- Global Variables --------- //

// -------- Functions --------- //

#ifdef SERVO
//initialize the servo
static inline void initTimer1Servo(void){
  TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM11);
  
  TCCR1B |= (1 << WGM12) | (1 << WGM13);
  TCCR1B |= (1 << CS11) | (1 << CS10);
  ICR1 = 4999;

  //enable servo signal pin.
  //DDRB |= (1 << PB1);
}

static inline void activateServo() {
  //Enable the servo pin.
  DDRB |= (1 << PB1);
}

static inline void deactivateServo() {
  //disable the servo pin.
  //lower power consumption.
  DDRB &= ~(1 << PB1);
}

// clicks == 0 then one click
// clicks == 1 then two clicks
//
void clickServo(uint8_t clicks) {

  //don't do more than three
  if(clicks > 3) {
    return;
  }
  
  //fire up the servo pin.
  activateServo();
  //make sure we're at servo reset postion
  OCR1A = SERVO_REST;
      
  for(uint8_t i = 0; i <= clicks; i++)
    {
      //perform click operation
      OCR1A = SERVO_PRESS;
      _delay_ms(SERVO_CLICK_MS);
      OCR1A = SERVO_REST;
      _delay_ms(SERVO_CLICK_WAIT_MS);
    }
  //shutdown the servo pin
  deactivateServo();
}
#endif

void initADC(void) {
  ADMUX |= (1 << REFS0);	/* reference voltage to AVCC */
  ADCSRA |= (1 << ADPS1) | (1<< ADPS2); /* ADC clock prescaler /64 */
  ADCSRA |= (1 << ADEN);		/* enable ADC */
}

//enable pins on block B for led output.
void initLED(void) {
  DDRB |= (1 << PB0) | (1 << PB5);
}

uint16_t readADC(uint8_t channel) {
  ADMUX = (0b11110000 & ADMUX) | channel;
  ADCSRA |= (1 << ADSC);
  loop_until_bit_is_clear(ADCSRA, ADSC);
  return (ADC);
}

void onLEDPortB(uint8_t led_pin) {
  PORTB |= (1 << led_pin);
}

void offLEDPortB(uint8_t led_pin) {
  PORTB &= ~(1 << led_pin);
}


int main(void) {
  // -------- Inits --------- //
  uint16_t adcValue; 

  uint16_t middleValue = 0;
  uint16_t highValue = 0;
  uint16_t lowValue = 0;
  uint16_t noiseVolume = 0;

  uint8_t padding = INITIAL_PADDING;
  uint8_t noiseState = C_NOTHING;

  uint8_t warmup_delay_index = WARMUP_DELAY_COUNT;
  
  initLED();
  initADC();
#ifdef SERVO
  initTimer1Servo();
#endif
  
#ifdef DEBUG
  initUSART();
  printString("OK");
#endif
  
  // ------ Event loop ------ //
  while (1) {
    
      
    adcValue = readADC(PC0);

    middleValue = adcValue + ROUND_SHIFT(middleValue);
    if(adcValue >= (middleValue >> 4)) {
      //reading is above middle... add it to the high value.
      highValue = adcValue + ROUND_SHIFT(highValue);
    }
    if(adcValue <= (middleValue >> 4)) {
      //reading is below the middle .. add it to the low value.
      lowValue = adcValue + ROUND_SHIFT(lowValue);
    }

    //Amplitude of the voltage signal.
    //Noise volume is unsigned (and don't let it overflow)
    if(highValue > lowValue + padding){
      noiseVolume = (highValue - lowValue) + padding;
    } else {
      noiseVolume = 0;
    }

    //Check to see if noiseVolume is above
    //--------------------------------------------------
    // HIGH REGION
    //--------------------------------------------------
    if(noiseVolume > NOISE_THRESHOLD_HIGH)
      {
	//previous state C_NOTHING..
	if(noiseState == C_HIGH)
	  {
	    //was high, is high.
	  }
	else if(noiseState == C_NOTHING)
	  {
	    //was nothing is high
	    //shouldn't get here.
	  }
	else if(noiseState == C_LOW)
	  {
	    //was low is high
	    onLEDPortB(PB0);
#ifdef SERVO
	    if(warmup_delay_index == 0)
	      {
		clickServo(0);
	      }
#endif
	    
	  }
	//Setting noiseState to HIGH...
	noiseState = C_HIGH;
      }
    //--------------------------------------------------
    // LOW REGION
    //--------------------------------------------------    
    else if (noiseVolume > NOISE_THRESHOLD_LOW)
      {
        if (noiseState == C_HIGH)
	  {
	    //was high is low.
	    //turn off led indicating that pump is on.
	    offLEDPortB(PB0);
#ifdef SERVO
	    if(warmup_delay_index == 0)
	      {
		//dbl click servo to indicate pump is transitioning to off.
		clickServo(1);
	      }
#endif
	  }
	else if(noiseState == C_NOTHING)
	  {
	    //was nothing is low.
	    //turn on led PB5
	    onLEDPortB(PB5);
	  }
	else
	  {
	    //was low is low.
	  }
	
	noiseState = C_LOW;
      }
    //--------------------------------------------------
    // BELOW LOW REGION
    //--------------------------------------------------    
    else
      {
	if(noiseState == C_HIGH)
	  {
	    //was high is nothing
	  }
	else if (noiseState == C_NOTHING)
	  {
	    //was nothing is nothing.
	  }
	else if (noiseState == C_LOW)
	  {
	    //was low is nothing
	  }
	
	offLEDPortB(PB0);
	offLEDPortB(PB5);
	noiseState = C_NOTHING;
      }

#ifdef DEBUG
    //    printString("adcValue\t");  
    printWord(adcValue);   
    printString("\t");  

    //printString("lowValue\t"); 
    printWord(lowValue);  
    printString("\t");  

    //printString("middleValue\t"); 
    printWord(middleValue); 
    printString("\t"); 
    
    //printString("highValue\t");  
    printWord(highValue);   
    printString("\t");  

    //printString("noiseVolume\t");  
    printWord(noiseVolume);   
    printString("\n");
#endif
    
    //initial action during warmup.
    if(warmup_delay_index > 0)
      {
	//during warmup, go through the motions, but don't trigger servo.
	onLEDPortB(PB0);
	_delay_ms(CYCLE_DELAY_MS);
	offLEDPortB(PB0);
	warmup_delay_index--;
      }
    else
      {
	_delay_ms(CYCLE_DELAY_MS);
      }
  }                                                  /* End event loop */
  return (0);                            /* This line is never reached */
}
